public class Circulo extends Ventana {
    private int radio;
    public Circulo(String nombre, int r) {
        super(nombre);
        this.radio = r;
    }
    public int getRadio() {
        return radio;
    }
    public void setRadio(int radio) {
        this.radio = radio;
    }
    public float area()
    {
        return (float) (3.14*radio*radio);
    }
}
