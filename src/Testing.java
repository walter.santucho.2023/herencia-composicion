import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;

public class Testing
{
    @Test
    public void testAreaRectangulo()
    {
        Rectangulo ventanaRectangular = new Rectangulo("Word", 3,4);
        int resultado;
        resultado = ventanaRectangular.area();
        assertEquals(12, resultado);
    }

    @Test
    public void testAreaCirculo()
    {
        Circulo ventanaCircular = new Circulo("Foxit", 5);
        float resultado;
        resultado = ventanaCircular.area();
        assertEquals(78.5, resultado, 0.1);
    }
}
