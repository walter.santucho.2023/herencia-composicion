public class Rectangulo extends Ventana {
    private int ancho;
    private int largo;
    public Rectangulo(String name, int a, int l) {
        super(name);
        this.ancho = a;
        this.largo = l;
    }
    public int area()
    {
        return ancho*largo;
    }
}
